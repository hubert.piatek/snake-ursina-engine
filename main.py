from ursina import *
from random import randint
from klasy import *
from bledy import *

try:
    aplikacja = Ursina()
except:
    raise BladUrsiny

try:
    gra = Gra()
except:
    raise BladGry

try:
    camera.orthographic = True # kamera w 2d
    camera.position = (0, 0, -2) # oddalenie kamery
except:
    raise BladKamery


aplikacja.run()
