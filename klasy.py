from ursina import *
from random import randint

class Gra:
    def __init__(self):
        plansza = Plansza()
        jablko = Jablko(parent=plansza)
        licznik_jablek = Text(text='')
        gracz = Gracz(parent=plansza, licznik=licznik_jablek, jablko=jablko)

class Plansza(Entity):
    def __init__(self):
        super().__init__()
        self.model = 'quad' # kwadrat
        self.color = color.gray
        self.scale = (25, 25) # rozmiar
        self.position = (0, 0, 0)

class Jablko(Entity):
    def __init__(self, parent):
        super().__init__()
        self.parent = parent
        self.model = 'circle'
        self.color = color.red
        self.scale = 0.05
        self.position = (0.1, 0.1, -0.5)
        self.collider = 'box'

class Gracz(Entity):
    def __init__(self, parent, licznik, jablko):
        super().__init__()
        self.model = 'quad'
        self.color = color.green
        self.scale = 0.05
        self.position = (0, 0, -0.5)
        self.collider = 'box'
        self.kierunekX = 0
        self.kierunekY = 0
        self.zjedzone_jablka = 0
        self.predkosc = 3
        self.segmenty_weza = []
        self.parent = parent
        self.licznik_jablek = licznik
        self.jablko = jablko

    def update(self):

        self.x += time.dt * self.kierunekX * self.predkosc # time delta time - czas który upłynął od poprzedniej kratki
        self.y += time.dt * self.kierunekY * self.predkosc

        kolizja = self.intersects()
        if kolizja.hit:
            self.zjedzone_jablka += 1
            self.licznik_jablek.disable()
            self.licznik_jablek = Text(text="Jablka: " + str(self.zjedzone_jablka), position=(0, 0.4), origin=(0, 0), scale=1.5, color=color.yellow, background=True)
            self.jablko.x = randint(-4, 4) * 0.1
            self.jablko.y = randint(-4, 4) * 0.1
            nowy_segment = Entity(parent=self.parent, model='quad', z=-0.029, color=color.green, scale=0.05)
            self.segmenty_weza.append(nowy_segment)

        for i in range(len(self.segmenty_weza) - 1, 0, -1):
            self.segmenty_weza[i].position = self.segmenty_weza[i - 1].position

        if len(self.segmenty_weza) > 0:
            self.segmenty_weza[0].x = self.x
            self.segmenty_weza[0].y = self.y

        if abs(self.x) > 0.47 or abs(self.y) > 0.47:
            for segment in self.segmenty_weza:
                segment.position = (10, 10)
            self.segmenty_weza = []
            self.zjedzone_jablka = 0
            print_on_screen("Przegrałes!", position=(0, 0), origin=(0, 0), scale=2, duration=2)
            self.position = (0, 0)
            self.kierunekX = 0
            self.kierunekY = 0

    def input(self, key):
        if key == "d":
            self.kierunekX = 0.3
            self.kierunekY = 0
        if key == "a":
            self.kierunekX = -0.3
            self.kierunekY = 0
        if key == "w":
            self.kierunekX = 0
            self.kierunekY = 0.3
        if key == "s":
            self.kierunekX = 0
            self.kierunekY = -0.3

